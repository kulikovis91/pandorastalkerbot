require_relative 'modules/rsspoll'
require_relative 'modules/telegramGrowler'

class RssWatcher 
    def initialize(telegramGrowler)
        @growler = telegramGrowler
    end
    
    def update(e)
        message = "#{e.dc_creator}: #{e.title}. #{e.link}"
        puts message
        @growler.growl(message)
        @lastItem = e.title
    end
end

poll = RssPoll.new('http://stalker.openpandora.org/feed.rss')
growler = TelegramGrowler.new(ARGV[0])

poll.add_observer(RssWatcher.new(growler));

threads = []

threads << Thread.new { growler.start }
threads << Thread.new { poll.start }

threads.each { |thr| thr.join }