require 'telegram/bot'
require 'observer'
require_relative 'database'

class TelegramGrowler 
    include Observable
    
    def initialize(token)
        @db = Database.new 
        @token = token
    end
   
    def start
        Telegram::Bot::Client.run(@token) do |bot|
        	bot.listen do |message|
        		case message.text
        			when '/subscribe'
        				bot.api.send_message(
        					chat_id: message.chat.id, 
        					text: "Hello, #{message.from.first_name} you have been subscribed to the newsletter"
        				)
        				@db.AddChat(message.chat.id)
        			when '/unsubscribe'
        				bot.api.send_message(
        				    chat_id: message.chat.id, 
        				    text: "Bye, #{message.from.first_name}"
    				    )
        				@db.DeleteChat(message.chat.id)
        			when '/help'
        			    bot.api.send_message(
        				    chat_id: message.chat.id, 
        				    text: "Commands: /subscribe; /unsubscribe"
    				    )
        		end
        		changed
        		notify_observers(message.text)
        	end
        end
    end   
    
    def growl(message)
        @chats = @db.GetChats
        # is it normal to call a run() each time you need to send stuff
        Telegram::Bot::Client.run(@token) do |bot| 
            @chats.each do |chat|
        		if chat != '' then 
        			bot.api.send_message(chat_id: chat[0], text: message)
        		end
        	end
    	end
    end
end