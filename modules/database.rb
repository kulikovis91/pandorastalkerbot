require 'sqlite3'

class Database
    def initialize
       @db = SQLite3::Database.new "test.db"
    end
    
    def GetChats 
        return @db.execute("SELECT * FROM Chats")
    end
    
    def AddChat(id)
        @db.execute("INSERT INTO Chats (id) VALUES (?)", [id])
    end
    
    def DeleteChat(id)
        @db.execute("DELETE FROM Chats WHERE id = ?", [id])
    end
end