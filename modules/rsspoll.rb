require 'rss'
require 'open-uri'
require 'observer'

class RssPoll
    include Observable
    
    def initialize(url)
        @url = url
        @started = false
        @lastUpdate = Time.now
            #Time.xmlschema('2016-08-05T00:00:00+00:00')
    end
   
    def start
       @started = true
       pollLoop
    end
    
    def stop
       @started = false
    end
    
    def pollLoop
        while @started do
            open(@url) do |rss|
                feed = RSS::Parser.parse(rss)
                newPosts = feed.items.select { |i| 
                    i.pubDate > @lastUpdate 
                }
                newPosts.reverse.each do |post|
                    changed
                    notify_observers(post)  
                end
                @lastUpdate = feed.items[0].pubDate
            end
            sleep 30
        end
    end
end