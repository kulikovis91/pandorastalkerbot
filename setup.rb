require "sqlite3"

# Open a database
db = SQLite3::Database.new "test.db"

# Create a database
rows = db.execute <<-SQL
  create table Chats (
    id int
  );
SQL
